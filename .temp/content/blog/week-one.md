---
title: "Week 1:Introduction"
date: 2022-01-13T23:08:37+02:00
draft: false
---

This is the very first week of the Digital Fabrication courses. I am excited to start the learning process of prototyping and design, and all that it entails. I have some prior knowledge on the topics, mostly on Computer Aided Design, laser-cutting, 3D printing, and some basic programming. However I'm expecting to learn even more on these topics, as well as gaining knowledge on the topics I have no experience in. 

We started the week with a Digital Fabrication Studio lecture, followed by two Digital Fabrication I lectures later in the week. During those two lectures we went over the course structure and this week's assignments. The assignments are as follows:


* Create a GitLab account and use its CI tools to publish a website. 
* Link to your repository
* Link to the published website
* Add a picture and description of yourself, at least one paragraph of text. 

My GitLab repository can be found from the link on the left, and an introduction of myself from the 'About' link. 

## Documentation

This weeks assignments and tasks were mostly already familiar to me, however I most definitely needed a refresher. I have not used GitLab before, but I have used GitHub. Git isn't something I've used often though, so I have trouble remembering the necessary commands from the top of my head. I am also far away from a professional, or even an adequate coder, so in my explanation below I very well might misspeak or be completely wrong about how things work. If that is the case please let me know as I would always love to learn more, especially if I am wrong.

I created my GitLab account, and my very first repository, and named it Digital Fabrication. I added my SSH-key to my account, since I already had one saved on my computer, so it would be easier for me to push code I work on locally. I cloned the repository with SSH to my computer, 

``` bash
git clone git@gitlab.com:fiia.kitinoja/digital-fabrication.git
```

and then googled Hugo, as the instructors had recommended us to use that. I have no previous experience with the site, so I went on to their documentation, to learn how to use their website builder. I have previously installed iTerm, Oh My ZSH, and homebrew on my laptop, so I was able to bypass a lot of the initial setup. Following the instructions I installed hugo, 

``` bash
brew install hugo
```

after which I moved on to step 2, creating a new site. 

``` bash
hugo new site quickstart
```

After running the above command I quickly realised I had created a new subfolder called quickstart, where the hugo directory structure had been created, inside my previously made digital fabrication folder. This wasn't what I had intended so I deleted the folder. I ran a new command, 

``` bash
hugo new site .
```

with the intention of creating the structure inside the folder I was currently at. However that prompted an error message in the terminal, "Error: /Users/fiiakitinoja/projects/digfab-old already exists and is not empty." After that I thought it best to delete my repository, and start over. I deleted the folder from my computer, and the repository from my GitLab account, and re-created the folder from my terminal with the correct name. 

``` bash
hugo new site digital-fabrication
```

After that I created a new project in GitLab without a readme file, so that I was able to see the command line instructions. I followed the instructions to push an existing folder to the repository, to have my git set up. 

``` bash
git init 
git remote add origin git@gitlab.com:fiia.kitinoja/digital-fabrication.git
git add .
git commit -m "Initial commit"
git push
```

After that I looked through some themes, and decided to go with [Tranquilpeak](https://themes.gohugo.io/themes/hugo-tranquilpeak-theme/) as I liked its simplicity. The white text on the navigation part wasn't quite legible however in my opinion, so I decided to change the background. I made a quick graphic on Procreate using one of their default brushes, uploaded it in the images folder, and changed the cover image path to change it. I went through the config.toml file to make the necessary changes to make the site my own, and after that I was pretty much done with the initial set up of my website. Locally, that is. As I had been making the changes, I had started the hugo server on my terminal according to the instructions, 

``` bash
hugo server -D
```

So that I was able to view the changes as I made. The text editor I have been using so far is Visual Studio Code, so that is what I decided to go with here as well. After I was happy with the look of the website, I closed the server, saved my files, and pushed it to git. I am using lazygit extension, as I like the visuality of it. 

After that I needed to get my website live, so I went to my GitLab account and created a .gitlab-ci.yml file. During the lectures I had tried it, but it failed because my account was not verified. I verified it with my card and that took care of the error. I created the file again and this time my pipeline was successful. However my website wasn't working when I went to the URL. After a little bit of troubleshooting I realised it was because I had use the HTML template, instead of Hugo template. I deleted the old ci file, created a new one with the correct template and my website was up and live. 

// Edit: After testing out my website on my phone, I realised the SSL certificate wasn't working. I added in my custom domain since I already had one, to obtain a Let's Encrypt SSL certificate through GitLab. Hopefully it works now!