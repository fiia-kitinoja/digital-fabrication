---
title: "Final Project Week 1"
date: 2022-01-17T15:31:07+02:00
draft: false
---

Final project: Aquaponic herb garden

Initial steps:

* Write notes about how aquaponics work 
* Figure out how big of a tank I can have
* Calculate how big of a growbed I can have based on the amount of fish/shrimp in the tank
* Draw an initial sketch with rough dimensions
* Automation: what could it do?
