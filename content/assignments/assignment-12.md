+++
title = "Week 12 : Output devices"
+++

## Week 12 : Output devices

This week we were learning about Output devices, and the assignment was:

- Add an output device to a microcontroller board you've designed, and program it to do something.
- Measure the power consumption of an output device.
- Include a hero shot and source files of your board in your documentation.

I decided to try a simple DC motor as my output device. I found a submersible pump component in the lab, and I wanted to try and get that working with a board, as I think that would be useful for my final project. I already have a pump I was planning on using, however the flow of it is quite fast, and I'm not sure if it would be better to slow it down a bit. I am hoping to be able to control the speed of the DC motor inside the pump with PWM pins.

## First board

![Image 1: Board1 schem](/images/A12/F12-1.jpg)

![Image 2: Board1 PCB](/images/A12/F12-2.jpg)

The first board I designed for this week I was following our instructor's example during the lecture. This board had a ATtiny 412 microchip, and had traces and pins for connecting both a DC motor and an LED strip. I routed the traces for the PCB, milled it, and soldered it without any bigger issues.

When it came to testing the board though, I tried looking at the Fab Academy page for Output week for some example code. I grabbed the code from Neil's example[hello.H-bridge.44](http://academy.cba.mit.edu/classes/output_devices/H-bridge/hello.H-bridge.44), but it was looking very complicated. When I tried to program the board though the programmer I had wasn't working, so I wasn't able to try it out. When I got another programmer that was actually working, I had decided to go a different route.

## Adrianino

Neil had mentioned Adrián Torres' project called [Adrianino](https://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html). It is a board designed to function similarly to an Arduino board. It uses ATtiny1416 chip, and has connection points for different input and output devices.

I followed his documentation, and created one myself, with slightly different routing.

![Image 3: Adrianino](/images/A12/F12-3.jpg)

After that I followed his instructions for the DC motor driver board, that connected to the 8-pin connection at the top, and would get its power from a 9V battery.

![Image 4: Adrianino DC motor](/images/A12/F12-4.jpg)

Code from Adrián.

```bash
//Fab Academy 2020 - Fab Lab León
//Motor
//Adrianino
//ATtiny1614  - ATtiny1624
const int switch1Pin = 10;     // switch 1
const int motor1Pin = 0;      // H-bridge pin 0 (in2)
const int motor2Pin = 1;      // H-bridge pin 1 (in1)

void setup() {
    // set the switch pins as input pins and activate their internal pull up resistors
    // so they are not in a floating state because their default state is now HIGH
    pinMode(switch1Pin, INPUT);


    // set H-bridge pins as outputs:
    pinMode(motor1Pin, OUTPUT);
    pinMode(motor2Pin, OUTPUT);
  }

void loop() {
    // if switch1 is pressed, (=LOW because the unpressed 'pulled up' state is HIGH)

  if (digitalRead(switch1Pin) == LOW) {
      analogWrite(motor1Pin, 127); // set pin 1 of the H-bridge to 50% using PWM
      analogWrite(motor2Pin, 0);   // set pin 2 of the H-bridge to low state
    }
     // if neither of the switches are pressed, the motor will stand still
  else
      {
      digitalWrite(motor1Pin, LOW);   // set pin 1 of the H-bridge low
      digitalWrite(motor2Pin, LOW);   // set pin 2 of the H-bridge low
      }
  }

```

The code above wasn't working with the submersible pump I had, which has a DC motor inside, or with a normal DC motor that I grabbed from the lab for testing. I tested the button on the board to make sure that the button was working correctly, which it was.

```bash
/*
  Button

  Turns on and off a light emitting diode(LED) connected to digital pin 13,
  when pressing a pushbutton attached to pin 2.

  The circuit:
  - LED attached from pin 13 to ground through 220 ohm resistor
  - pushbutton attached to pin 2 from +5V
  - 10K resistor attached to pin 2 from ground

  - Note: on most Arduinos there is already an LED on the board
    attached to pin 13.

  created 2005
  by DojoDave <http://www.0j0.org>
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Button
*/

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 10;     // the number of the pushbutton pin
const int ledPin =  8;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}
```

After this I did a lot of troubleshooting, which I won't be documenting here, mostly because I've forgotten most of the steps I went through. I ended up milling and soldering five or six different boards, always finding some small error I did or when the microcontroller suddenly stopped working. Looking back now I think it might've been because I attached the battery in wrong, but I can't be sure. Anyway at that point I was getting so behind on other weeks assignments, so I decided to go with something different.

I wanted to focus on my final project, and decided to build a board that would work for the outputs I needed. I decided to try the 12V LED strip, that would be for lighting up my aquarium.

![Image 5: Aqua outputs](/images/A12/F12-5.jpg)

![Image 5b: Aqua outputsb](/images/A12/F12-5b.jpg)

I used the board from Kris' lecture as an example, and included a mosfet and all the other necessary components.

![Image 6: Aqua outputs 2](/images/A12/F12-6.jpg)

```bash
#define LED A3

void setup() {
  // put your setup code here, to run once:
pinMode(LED, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
digitalWrite(LED, HIGH);
}

```

The code I wrote for the board is extremely simple, however it worked!

![Image 7: Aqua outputs 3](/images/A12/F12-7.jpg)

There was another issue though, which was that every time I connected the board to the computer, it was getting burning hot. After doing some troubleshooting with one of the teachers, we figured out the issue was the regulator. When I was programming the board I didn't have the battery attached to the board, which means the power was coming through the UPDI connection and entering the regulator through the Voltage out pin. I managed to cut the traces and solder in a resistor for the LED strip, as well as a diode for the regulator to make sure current couldn't flow through the wrong way. That completely fixed the issue and the board didn't get hot anymore.

![Image 7: Aqua outputs 4](/images/A12/F12-8.jpg)
