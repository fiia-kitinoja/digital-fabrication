+++
title = "Week 7 : 3D Scanning and Printing"
+++

## Week 7: 3D Scanning and Printing

The assignment this week includes the following:

* Test the design rules of the 3D printers at Aalto Fablab.
* Design an object that can not be made using subtractive manufacturing.
* 3D print the object you designed.
* 3D scan something and (optionally) print it.
* Document your process in a new page on your website.

I have a 3D printer at home, so I wanted to use that for this weeks assignment. We've only had it for about four months or so, and there is still a lot I need to learn about 3D printing to truly master it, so I was quite happy to explore it for this assignment. 

## Printer

![Image 1: Creality Ender Pro 3D printer](/images/A7/A7_1.jpg)

The printer I have is the [Creality Ender 3 Pro 3D printer](https://www.creality.com/goods-detail/ender-3-pro-3d-printer). When me and my partner were looking for different printers we ended up going for the Ender 3, due to its great reviews. The Ender 3 Pro, and its sister Ender 3 and Ender 3 V2, are relatively cheap, while still being capable of producing very good quality prints. The ender 3 is also a very popular machine, which means the community around it is huge, and there is a lot of information about possible issues and ways to upgrade it. 

Specifications:

* Modeling Technology: FDM（Fused Deposition Modeling）
* Printing Size: 220 * 220 * 250mm
* Printing Speed: ≤180mm/s, normal 30-60mm/s
* Filament: 1.75mm PLA, ABS, Wood, TPU, Gradient color, carbon fiber, etc.
* Working Mode: Online or SD card offline
* File Format: STL, OBJ, AMF
* Slice Software: Cura, Repetier-Host, Simplify3D

The main downside, if you can call it that, is that when you receive the printer it will come in parts and has to be assembled. However due to the large community of Ender 3s, there are a lot of tutorials and instructions helping you on how to assemble it and minimise possible issues. We used [this](https://www.youtube.com/watch?v=me8Qrwh907Q) video to follow along, among some others. We were quite nervous about the assembly beforehand, but in the end it was surprisingly easy, and we didn't have any issues. 

![Image 2: Glass bed](/images/A7/A7_2.jpg)

So far the only upgrade for it we have done is a glass bed. The printer was shipped with a flexible magnetic bed, but lately we have had some adhesion issues, especially on the harder prints, so we decided to get the glass bed, which improves adhesion, and so far it has been working great. 

## Previous prints

![Image 3: Collection of previous prints](/images/A7/A7_3.jpg)

Here are a couple of previous prints that I have done with the printer. Mostly the quality is really good, but there are a few issues that I have experienced. [Here](https://www.simplify3d.com/support/print-quality-troubleshooting/) is a very good troubleshooting guide that helps you improve the quality of the prints.

## Assignment

#### Testing the design rules

In order to test the some of the design rules I went on to [thingiverse](https://www.thingiverse.com) to find a good design that tests the capabilities of the printer. 

![Image 4: Benchy](/images/A7/A7_4.jpg)

Benchy is probably one of the most popular 3D prints of all time, and is a good benchmark print as it tests many different often difficult aspects of 3D prints. The benchy above is one that I have printed on my 3D printer a while ago, so this time I wanted to print a different benchmark. 

![Image 5a: 3D printer test](/images/A7/A7_5_1.jpg)

![Image 5b: 3D printer test](/images/A7/A7_5_2.jpg)

I ended up choosing [this](https://www.thingiverse.com/thing:2806295) one. I was very happy with the quality of the print, and I feel like my printer did very well on the task. The filament I used wasn't maybe the best one to use, as it is slighty transparent, so the texts isn't very legible, especially in photos. 

The main issues that I noticed after looking carefully at the print, were the dimensions. The hole test, diameter test and the scale test were all +/- 0.1 mm from their supposed dimensions, which isn't too bad but is something I should take into consideration if I am printing something that needs a tight fit. 

![Image 6: Overhang backside](/images/A7/A7_6.jpg)

Other than that, the printer did really well on the overhang test, on the backside of the 80 degree overhang the surface quality wasn't that great, but otherwise it managed. The bridging on the 25mm bridge was also a bit droopy. I tried to take pictures, but as I mentioned the translucency of the filament makes it quite difficult to see detail. 

#### Designing a 3D printable object

It took me a really long time to figure out what I wanted to make using the 3D printer. There are so many resources to find 3D models for printing online, I've usually just downloaded them. The phrase 'that can't be made using subtractive manufacturing' was also throwing me off a bit. As I kept on thinking what I could make I always ended up thinking it wasn't specific enough for something that could only be made by 3D printing. I know I was definitely overthinking it as usual. The issue was also, that I wanted to print something useful, but couldn't think of anything. In the end I just ended up going with a 'ball in a cage' that is a typical whittling project that requires high skill. 

![Image 7: 3D Design](/images/A7/A7_7.jpg)

I used Fusion 360 for the design process, which wasn't overly complicated. I wanted to minimise the amount of support needed in the print, so I needed to figure out how to do the 'roof'. I couldn't do a flat roof as the printer can't print on air, so I decided to continue the columns diagonally and bring them together at the tip. This made the angle roughly 45 degrees, which should be easily doable by the printer based on the overhang test. 

#### 3D Printing the object

After exporting the model as an .stl file, I was ready to bring it in to my slicer software. Slicer software cuts the 3D model into individual layers and puts out GCode which essentially are instructions for the printer on how to move. There are a lot of settings that can be tweaked depending on whats needed, but I've mainly been using the recommended settings, as so far they have been working out well for me. 

![Image 8: Cura](/images/A7/A7_8.jpg)

The Slicer that I like to use is Ultimaker Cura. We have connected a raspberry pi into the printer, and installed Octoprint, which makes it very easy to send files to be printed without having to always use usb sticks or SD cards. 

![Image 9: 40 degree supports](/images/A7/A7_9.jpg)

I used the recommended settings for the low quality print. As you can see from the image above, Cura added supports all over the design, which isn't what I wanted. So I went in to the custom settings and searched for supports. There I was able to find a setting for support overhang angle. The angle was set at 35, which means any overhang over 35 would have supports added. Based on my 3D printer test I deduced that overhang up to 70 degrees was fine, so I changed the setting to 70, and sliced the model again. 

![Image 10a: Sliced model](/images/A7/A7_10_1.jpg)

![Image 10b: Sliced model](/images/A7/A7_10_2.jpg)

With this setting I was able to reduce the supports to minimum, and now they are only under the ball keeping it up. 

Next I sent the file to the printer through Octoprint, and the printer started heating up the bed and the extruder! The model ended up being a good bit bigger than I realised, so the print actually took almost 6 hours to print. 

![Image 11: Octoprint](/images/A7/A7_11.jpg)

In addition to OctoPrint we also have a small camera attached to the raspberry pi and Octolapse installed, which takes images of the print after each layer, and creates a nice timelapse of the print. In addition to that you can also use the camera to monitor the print to make sure nothing goes wrong, and if the print fails you can cancel the print remotely so that no extra filament is lost. 

{{< video src="/images/A7/A7_V2.webm" type="video/webm" preload="auto" >}}

Above you can see the timelapse of my first attempt at printing the ball in the cage. As you can see it ended up failing at the very end, where I assume one of the columns got stuck in to the nozzle, and as it moved away it broke the column. I ended up breaking the rest of the columns as well while I was trying to get the print off the bed, so I realised I had designed them too thin. 

![Image 12: V2 Model](/images/A7/A7_12.jpg)

For the second version I made the columns slightly thicker. I also decided to scale down the model in Cura, as I didn't want to wait another 6 hours for the print to fail again. Otherwise I kept the settings the same. The printing time was about 2.5 hours, which wasn't too bad. 

![Image 13: V2 Model Print](/images/A7/A7_13.jpg)

{{< video src="/images/A7/A7_V3.webm" type="video/webm" preload="auto" >}}

{{< video src="/images/A7/A7_V1.webm" type="video/webm" preload="auto" >}}

#### 3D Scanning

![Image 14a: Artec Leo](/images/A7/A7_14_1.jpg)

![Image 14b: Artec Leo](/images/A7/A7_14_2.jpg)

For 3D scanning I used the Artec Leo 3D scanner that is found in the Aalto Fablab. It is a very easy to use, high quality wireless scanner, and scanning the object was easy. For my object I chose a wooden joinery block to scan, as I thought that would be interesting to try and 3D print. 

The way the scanning works, is to place markers on the objects, and putting them down on an empty, flat surface. I didn't want to mark the surface of the blocks, so I used masking tape, and drew x's on the tape as markers. I used a black marker, which ended up being not a good idea. I don't fully understand how 3D scanning works, but I know it has something to do with light bouncing back from the objects. The colour black will absorb most of the light instead of reflecting it back, which makes sense that it took a really long time to fully register the markers.

![Image 15: 3D scan](/images/A7/A7_15.jpg)

After placing on markers you can start a new project on the scanner, then begin the scan. By pointing at the table surface for a couple of seconds will help the scanner to recognise it as a surface, and ignore it for the print. After that we can just point the scanner to the object from different angles until the surfaces on the scan all look green. Then it is time to rotate the objects to be able to cover the surfaces that were previously laying against the table and repeat the process. In theory two scans should be enough to cover all the angles and surfaces, but it is a good idea to do at least three, to make sure as much area as possible is covered. 

After doing the scanning from all angles the scanner can be connecter to the computer to upload the files. The point cloud files need to be sewn together and any possible issues fixed, which happens through the Artec Studio program. Unfortunately I didn't have time to do that before the documentation was due, but I will add it here later. 