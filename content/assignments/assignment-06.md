+++
title = "Week 6 : Electronics production"
+++

## Week 6: Electronics production

This week our assignment was as follows:

* Characterize the design rules for the PCB production process at the Aalto Fablab.
* Make an in-circuit programmer. Create tool paths for the milling machine, mill it and stuff (solder components) the board.
* Test the board and debug if needed.
* Document the process in a new page on your documentation website using pictures and text.
* Submit the link of the page.

I have no experience at all with electronics, and actually know very little about how they work. For that reason I was quite nervous about this week, as for the previous weeks assignments I've had at least some kind of prior knowledge of the topic. Looking back though it wasn't as hard as I imagined, and ended up having a lot of fun during this week. I am excited to learn more about producing and designing electronic components.

#### Step 1: Downloading the files

I was able to find the necessary design files fot the UPDI D11C programmer [here](http://pub.fabcloud.io/programmers/summary/). We will be using CopperCam software for generating toolpaths, so the gerber files were what I needed. 

![Image 27: Gitlab repo](/images/A6/A6-27.jpg)

I went to the gitlab repository, and downloaded the whole repository on my computer. I quickly realised the gerber files weren't in there already, so I needed to generate them.

![Image 28: ci-generated files](/images/A6/A6-28.jpg)

The repository README-file had a link to the website with CI-generated files.

![Image 29: downloading gerbers](/images/A6/A6-29.jpg)

There I was able to find the link for the gerber files, and downloaded them as well.

#### Step 2: Generating toolpaths

![Image 1: Open gerber files](/images/A6/A6-1.jpg)

After downloading the necessary files I opened them in CopperCam. There are three different gerber files. The first one is a cut file, and the second engraving file. However it is best practice to include the cut line in the engraving file, so that there is no need for aligning two layers. This is the case for this file, so the only file I need is the middle one. The last file is a mask file, which is for a different use case than here.

![Image 2: highlighted track](/images/A6/A6-2.jpg)

After opening the file a dialogue box appears, asking me to confirm that the highlighted track defines a card contour path. I am not completely sure what it means, but I think it should be highlighting the cut line, and asking if it is showing correctly?

![Image 3: board dimensions](/images/A6/A6-3.jpg)

Next step is to check that the dimensions are correct. The thick black outline shows the cut, and there is a thinner line offset by 1mm. To check the dimensions I followed the directions, and opened the board dimensions dialogue box. I set a reframe margin of 1mm, and clicked OK. Nothing changed on the board, which was what happened on the video, so I assume my dimensions are correct. Thinking back now I should've tried a different size margin to see the difference to understand this tool better. But I'll have to remember to do that next time. 

![Image 4: origin point](/images/A6/A6-4.jpg)

Next step is to reset origin point. A white cross cursor indicates the current origin point, which can partly be seen in the lower left corner. I select origin point tool and set position to 0,0.

![Image 5: tool library](/images/A6/A6-5.jpg)

Tool library shows the available tools and their specifications, that should be set correcly for all of them. For this project we will use Tool 2 for engraving, and Tool 3 for cutting.

![Image 6: active tools](/images/A6/A6-6.jpg)

Next by selecting active tools dialogue window we can specify the tool settings for our specific file. For this I didn't change many settings, as other people had already been cutting the exact same project. However the important bits I should note, are choosing the correct tools for each task, engraving and cutting. Our engraving tool is .2-.5mm, so engraving speed is fine at 10mm/s. However if we were using the smallest engraving bit, which is .1-.2mm, 10mm/s would break the tool, and it should be set to 5mm/s. 

![Image 30: plate thickness](/images/A6/A6-31.jpg)

I was using some leftover copper covered plate, and measured it to see how thick the cut should be. It measured out to 1.64mm, and according to our instructor we should set the cut depth 0.2 or 0.3 mm higher than that. I set the cutting depth to 1.95.

![Image 7: set contours](/images/A6/A6-7.jpg)

Next was time to calculate the contours, where the program calculates the optimal toolpaths. I didn't change any settings here, but in the future I probably would be if I am milling something more complicated. Regardless our instructor mentioned four rounds being the optimum, but there certainly may be times when the optimum number would be different.

![Image 8: preview](/images/A6/A6-8.jpg)

After clicking okay the software will show preview of the engraving and cuts. 

![Image 9: toolpaths](/images/A6/A6-9.jpg)

By clicking the preview tool we can change between the preview overview and tool paths overview.

![Image 10: notable issue preview](/images/A6/A6-10.jpg)

![Image 11: notable issue toolpaths](/images/A6/A6-11.jpg)

The preview is an important step in the process, as it can show possible issues in the mill. The above pictures show that two lines are still connected in the preview, even though they shouldn't be. 

![Image 13: track apertures after](/images/A6/A6-13.jpg)

There is not enough space for the tool to cut, so to fix the issue we can make the lines a little bit smaller. I rightclicked one of the ligh green lines and chose edit all identical lines. In the opened window I set the width from 0.4mm to 0.38mm, and that fixed the issue.

![Image 14: track apertures after](/images/A6/A6-14.jpg)

![Image 15: issue fixed preview](/images/A6/A6-15.jpg)

![Image 16: issue fixed toolpaths](/images/A6/A6-16.jpg)

![Image 17: engrave at centres](/images/A6/A6-17.jpg)

The last thing I changed was the text. I selected all the text and selected engrave at centerlines, which would look better than engraving their outlines, since I wasn't hatching out the unneccessary material on the outside.

![Image 18: file output](/images/A6/A6-18.jpg)

![Image 19: exported files](/images/A6/A6-19.jpg)

Then it is time to output the files for milling! CopperCam will export two EGX files, one for engraving, and one for cutting. It will automatically put the tool numbers on the name of the files, which makes it easy to distinguish which is which. 

#### Step 3: Machine setup

![Image 20: VPanel open](/images/A6/A6-20.jpg)

Next I opened VPanel, which is the program that controls the SRM-20 machine at our FabLab. I made sure the machine was on before that. 

![Image 30: tool tips](/images/A6/A6-30.jpg)

Here are the tooltips we are using with the machine. The first two on the left are engraving tips, the next one is a cutting tip, and the rest are drilling tips.

![Image 32: plate set on the bed](/images/A6/A6-32.jpg)

![Image 33: double sided tape](/images/A6/A6-33.jpg)

 I attached the correct engraving tooltip first by using the allen key attached to the machine. Then I fixed the plate on the bed by using some double sided tape.

![Image 21: z origin reset](/images/A6/A6-21.jpg)

![Image 22: xy origin reset](/images/A6/A6-22.jpg)

The main thing to set in VPanel before milling is resetting the x, y, and z axis. I set the z axis first by using the arrows on VPanel. The tool tips can break very easily with force, so it is crucial to make sure you aren't trying to push the tip down lower than the plate surface. I lowered the tip relatively close to the surface using the continuous step, and then used x100 steps to have more control of the movement. When the tip was low enough I loosened the screw and let the tip fall down to the surface gently. Then holding the tip I tightened the screw. I went back to VPanel and set a new origin point for the z axis. Next I moved the bit higher by a few millimiters so that it could move easily, and moved the tip to the lower left corner of the plate, and set the origin points for x and y axis. Then I was ready mill and clicked cut. 

![Image 23: open engrave file](/images/A6/A6-23.jpg)

![Image 24: SRM-20 error message](/images/A6/A6-24.jpg)

I selected the engraving file and opened it. An error message opened in VPanel, but we were instructed that this is to be expected and to just click resume to start the mill. 

#### Step 4: Milling

![Image 34: failed mill](/images/A6/A6-34.jpg)

![Image 35: reason for failing](/images/A6/A6-35.jpg)

My first mill failed as you can see in the photo above. The engrave wasn't low enough, and it didn't cut through all the copper. I suspected the culprit to be that the plate wasn't secured to the bed well enough, because the side where I had started engraving was rised a bit from the edge. I had only put tape on the lower right side of the plate, so I decided to cut there with the same settings. 

![Image 37: successful mill](/images/A6/A6-37.jpg)

This time I had no issues with the mill, and the board came out looking perfect. 

![Image 38: cutting tool tip](/images/A6/A6-38.jpg)

![Image 39: resetting z axis](/images/A6/A6-39.jpg)

![Image 25: z origin reset for cutting](/images/A6/A6-25.jpg)

![Image 26: open cut .egx file](/images/A6/A6-26.jpg)

Next I moved on to cutting the board out of the plate. I changed the engraving tool tip to the cutting tool tip, and repeated the steps to setting up the machine. I reset only the z axis, as I wanted the x and y axis to remain the exact same so that the cut would be exactly where I needed it to be. 

![Image 40: successful cut](/images/A6/A6-40.jpg)

![Image 41: milled board](/images/A6/A6-41.jpg)

![Image 43: inspecting engraving](/images/A6/A6-43.jpg)

The cut was also successful, and inspecting it I couldn't see any potential issues on the board, so I was ready to move on to soldering!

#### Step 5: Parts

The parts needed for the programmer can be found on the [interactive BOM](http://pub.fabcloud.io/programmers/programmer-updi-d11c/ibom/) website on the Fablabs gitlab repository. The parts needed included the following:

![Image 45: 2x3 pin header connector](/images/A6/A6-45.jpg)

2x3 vertical pinheader connector

![Image 46: 2x2 pin header connector](/images/A6/A6-46.jpg)

2x2 vertical header connector

![Image 44: arm chip](/images/A6/A6-44.jpg)

ATSAMD11C arm chip

![Image 47: two 4.99K resistors](/images/A6/A6-47.jpg)

2 resistors

![Image 48: 1UF capacitor](/images/A6/A6-48.jpg)

1 micro farad capacitor 

![Image 49: linear regulator](/images/A6/A6-49.jpg)

Voltage converter 5v to 3.3 volts

![Image 50: all the parts](/images/A6/A6-50.jpg)

Finally all the parts together.

#### Step 6: Soldering

![Image 52: soldering setup](/images/A6/A6-52.jpg)

Fablab in Aalto has great soldering stations, and the magnifying glass with lights was a great help to see the tiny components. 

![Image 53: isopropyl alcohol for cleaning](/images/A6/A6-53.jpg)

I used isopropyl alcohol to clean the surface of the board of any possible dust or debris.

![Image 54: testing board 1](/images/A6/A6-54.jpg)

![Image 55: testing board 2](/images/A6/A6-55.jpg)

I use the machine above to test the copper connections on the board, to make sure I hadn't missed any lines that were connected that shouldn't be.

![Image 56: arm chip](/images/A6/A6-56.jpg)

I started with the arm chip and used a microscope to find which edge was lightly slanted. I used that to orientate the chip correctly onto the board. This was my first time soldering, and I followed the instructions to first put down a blob of solder to the board, and then grap the part gently with my tweezers. I would then re-melt the solder with my soldering iron, and hold down the part while removing the iron so that one corner would be attached. Then I melted solder on the other legs to fully connect the part. 

![Image 57: halfway through soldering](/images/A6/A6-57.jpg)

![Image 58: finished soldering](/images/A6/A6-58.jpg)

Above you can see the final result. Please don't judge me too harshly, as this was my first time trying to solder. I think in places I used too much solder, especially on the 2x3 header, capacitor, and the regulator. I haven't had time to test the board yet, but I hope that it works. However if it doesn't I'm happy to try the process again as I found the whole week very fun beginning to learn a completely new skill. 
