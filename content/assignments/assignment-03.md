+++
title = "Week 3 : Project Management"
date = 2022-01-13T23:08:37+02:00
+++

This week our assignment are as follows:

* Create a page for the project management assignment on your website.
* Describe how you created your website on that page.
* Describe how you use Git and GitLab to manage files in your repository.

First I will describe how I created my website. My process includes a lot of trial and error, as I have found that is the best way for me to learn and retain the information for the future as well. I might forget some steps in this description, but I try to be as thorough as possible. 

I created my GitLab account, and my very first repository, and named it Digital Fabrication. I added my SSH-key to my account, since I already had one saved on my computer, so it would be easier for me to push code I work on locally. I cloned the repository with SSH to my computer, 

``` bash
git clone git@gitlab.com:fiia.kitinoja/digital-fabrication.git
```

and then googled Hugo, as the instructors had recommended us to use that. I had no previous experience with the site, so I went on to their documentation, to learn how to use their website builder. I have previously installed iTerm, Oh My ZSH, and homebrew on my laptop, so I was able to bypass a lot of the initial setup. Following the instructions I installed hugo, 

``` bash
brew install hugo
```

after which I moved on to step 2, creating a new site. 

``` bash
hugo new site quickstart
```

After running the above command I quickly realised I had created a new subfolder called quickstart, where the hugo directory structure had been created, inside my previously made digital fabrication folder. This wasn't what I had intended so I deleted the folder. I ran a new command, 

``` bash
hugo new site .
```

with the intention of creating the structure inside the folder I was currently at. However that prompted an error message in the terminal, "Error: /Users/fiiakitinoja/projects/digfab-old already exists and is not empty." After that I thought it best to delete my repository, and start over. I deleted the folder from my computer, and the repository from my GitLab account, and re-created the folder from my terminal with the correct name. 

``` bash
hugo new site digital-fabrication
```

After that I created a new project in GitLab without a readme file, so that I was able to see the command line instructions. I followed the instructions to push an existing folder to the repository, to have my git set up. 

``` bash
git init 
git remote add origin git@gitlab.com:fiia.kitinoja/digital-fabrication.git
git add .
git commit -m "Initial commit"
git push
```

After that I looked through some themes, and decided to go with [Tranquilpeak](https://themes.gohugo.io/themes/hugo-tranquilpeak-theme/) as I liked its simplicity. The white text on the navigation part wasn't quite legible however in my opinion, so I decided to change the background. I made a quick graphic on Procreate using one of their default brushes, uploaded it in the images folder, and changed the cover image path to change it. I went through the config.toml file to make the necessary changes to make the site my own, and after that I was pretty much done with the initial set up of my website. Locally, that is. As I had been making the changes, I had started the hugo server on my terminal according to the instructions, 

``` bash
hugo server -D
```

So that I was able to view the changes as I made. The text editor I have been using so far is Visual Studio Code, so that is what I decided to go with here as well. After I was happy with the look of the website, I closed the server, saved my files, and pushed it to git. I am using lazygit extension, as I like the visuality of it. 

After that I needed to get my website live, so I went to my GitLab account and created a .gitlab-ci.yml file. During the lectures I had tried it, but it failed because my account was not verified. I verified it with my card and that took care of the error. I created the file again and this time my pipeline was successful. However my website wasn't working when I went to the URL. After a little bit of troubleshooting I realised it was because I had use the HTML template, instead of Hugo template. I deleted the old ci file, created a new one with the correct template and my website was up and live. 

After testing out my website on my phone, I realised the SSL certificate wasn't working. I added in my custom domain since I already had one, to obtain a Let's Encrypt SSL certificate through GitLab. I later found out that the issue was because of a dot in my username (fiia.kitinoja). I have since changed my username to fiia-kitinoja, but have kept the domain since I added it already. 

## Learning more about Hugo

Quickly after setting up the first version of the website I realised that the template I had chosen was good for a blog, but since in this course we need to create multiple static pages, for example a page for the final project as well, I realised I needed to edit my site. Rather than editing the template, I decided to try and build the site myself using hugo. Kris advised me to look up the recorded lecture from previous year's Digital Fabrication course on hugo, which I did. In addition to watching the video I also referenced hugo's documentation, to get more information on specifics I didn't understand. 

I started by moving the existing directory structure to a hidden .temp folder so that I could start from a clean slate without losing any of my previous work. After that I created a new hugo website. 

``` bash
mkdir .temp
mv ./* .temp/
hugo new site . --force
```

After that I removed the unnecessary folders so that the directory structure is cleaner. After that I edited the config file with my website name and base URL. Then I created a index.html file in the layouts folder, and added some basic HTML, that included a header, navigation, main body of text, and a footer. Within the html I used hugo parameters that refer to the config file for the base URL and the site title, as instructed in the video. Example below.

``` bash
    <title>{{ .Site.Title }}</title>
``` 

Following further with the video I created a subfolder called partials inside the layouts folder, and created a nav.html file. I then moved the navigation HTML code within that file, and referenced it within the index.html.

``` bash
            {{ partial "nav.html" . }}
``` 

I did the same for the header and the footer, replacing the "nav.html" with the corresponding filenames. I copied over the documentation page from assignment 1, created new pages for about and final project sections, and made a subfolder in layouts called _default. Within that I created single.html file as per instructions in the video. This is pretty much where the video left off, but I had gotten into the groove and wanted to keep going. I wanted to learn more about how hugo work, and stumbled upon [this](https://www.jakewiesler.com/blog/hugo-directory-structure) blog post. It explained very well the main components of hugo, and gave me some more insight. 

At this point all my links were working, however they were missing some content. I had filled in the markdown file for my Week 01 assignment page, but the content wasn't displaying. I felt I was missing a connection between the layout file, and the content. After digging through a little bit of the hugo documentation, I found the line of code I needed to add. 

``` bash
        <main>
            {{ .Content }}
         </main>
``` 

I am not 100% sure if the main-tags need to be added, but I felt it wouldn't hurt. After adding the line the files connected and the content was now showing on the page. At this point I was happy and felt that I understood how the directory structure works, and I wanted to move on to customising my site. 

## Bootstrap

I watched more of last year's lectures, this time about using bootstrap for components. I followed the instructions on the video and downloaded the zip file from Bootstrap's website, and picked the two needed files, bootstrap.min.css and bootstrap.min.js and saved them in my directory structure, under static > assets > css/js respectively. I also created a new css file for my own customisations. I then added code in my HTML to link the files.

``` bash
        <link rel="stylesheet" href="{{ .Site.BaseURL }}assets/css/css.css">
        <link rel="stylesheet" href="{{ .Site.BaseURL }}assets/css/bootstrap.min.css">
``` 

``` bash
        <script src="{{ .Site.BaseURL }}assets/js/bootstrap.min.js"></script>
``` 

I used bootstrap mainly for my navigation bar, containers, and a hero component on my websites main page. I customised these components to suit my website in my own css file by adding a custom class to the bootstrap defined classes and referencing it in my css code. Examples below.

``` bash
    <div class="custom-container">
        <nav class="navbar navbar-expand-lg navbar-light navigation-bar">
            <div class="container-xxl nav-container">
                <a class="navbar-brand" href="{{ .Site.BaseURL  }}">Fiia Kitinoja</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active btn nav-button" aria-current="page" href="{{ .Site.BaseURL  }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn nav-button" href="{{ .Site.BaseURL  }}about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn nav-button" href="{{ .Site.BaseURL  }}final-project/week-1-2">Final Project</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle btn nav-button" href="{{ .Site.BaseURL  }}assignments" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Weekly Assignments
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="{{ .Site.BaseURL  }}assignments/assignment-01">Week 01: Introduction</a></li>
                        <li><a class="dropdown-item disabled" href="{{ .Site.BaseURL  }}assignments/assignment-02">Week 02: Principles and Practices</a></li>
                        <li><a class="dropdown-item disabled" href="#">Week 03: Project Management</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn nav-button" href="{{ .Site.BaseURL  }}resources">Resources</a>
                    </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

--------------

.custom-container{
    width:100%;
    padding:0;
    background-color: #BCE6DB;
    border-bottom: 20px solid #8bc9b8;
    z-index: 100;
    -webkit-box-shadow: 0px 0px 11px -3px rgba(0,0,0,0.05); 
    box-shadow: 0px 0px 11px -3px rgba(0,0,0,0.15);
  }


.header{
    margin-bottom: 0;
}

.navigation-bar{
    margin-top: 0;
}

.content-container{
    background-color: #bce6db69;
    margin-bottom: 30px;
    padding: 20px;
    border-bottom-left-radius: 25px;
    border-bottom-right-radius: 25px;
    border: 5px solid #8bc9b8;
    border-top: none;
    max-width: 90%;
    -webkit-box-shadow: 7px 7px 10px 1px rgba(0,0,0,0.05); 
    box-shadow: 7px 7px 10px 1px rgba(0,0,0,0.05);
    z-index: 1;
}

.nav-container{
    max-width: 90%;
}
``` 

## Using Git and GitLab for file management

I use Git locally on my computer, and edit the text files with Visual Studio Code program, which is familiar to me from before. I use the directory structure provided by Hugo, to take advantage of their static website generator. In my directory structure I have three main folders that I use, content, layouts, and static. 

Inside content-folder I have saved all the text based content I will be showing on my website. The folder is divided into sub-folders based on the type of content it is, for example assignments and final projects. Every page has it's own markdown file, in which I will type out the text using markdown, and then reference the name of the file in my code to retrieve it. 

In layouts I have all my code files. It is subdivided into index, partials, _default, and the type of pages I have on my contents folder, assignments and final project. These files will determine how my website will look. 

In static folder I have saved all my static files, like images and assets. It is where I save my CSS files as well. 

After doing any changes to my website and saving, I will commit the changes in git with a brief commit to describe my actions. This will be useful in the future if I ever need to go back in time to a previous version. I use lazygit on my terminal, as I like the visual nature of the tool, See screenshot below. However I also know how to commit and push to gitlab regularly using the terminal. 

<img src="/git.png" alt="lazy git terminal" width="600">

After committing the change I will push it to GitLab where it can be viewed in my public repository. 

Hopefully I covered everything necessary in this post. Thank you for reading!