+++
title = "Week 10 : Embedded programming"
+++

## Week 10: Embedded programming

This week we were looking at embedded programming, which refers to the code and programming language within an *embedded system*. An embedded system is a computer system, consisting of a processor, memory, and input and output devices, which is embedded within a larger device. Most of our electronics today are controlled by embedded systems. 

This week builds upon the previous electronics weeks, production and design. During the electronics design week I created a [binary counter]("{{ .Site.BaseURL  }}assignments/assignment-08"), where I jumped onto the embedded programming as I was excited to get it to work. I talked a little about the code and posted it on the documentation page for that week. 

On this week's documentation my plan is to talk more about the binary counter counter code and the different blocks of the code, as well as try to program something different using the button and the LEDs. In addition to that I borrowed a [Hello  D11C Usb-C programmer](https://gitlab.fabcloud.org/pub/programmers/hello-d11c-usb-c) from our instructor Kris, so that I could try some coding on the D11C microchip as well. 

## AVR vs. ARM

The two families of microcontrollers we are focusing on in Fablab are AVR and ARM. Within AVR there are many different series, the one I have used is the 1-series with controllers ATtiny412 and ATtiny1614. These are 8-bit microcontrollers. Someone who has actually studied computer science could definitely explain this better than I can, but essentially it refers to the controllers processing power. Bits are the most basic units in computing that are used to run programs, and can have a value of either 0 or 1 (or on/off, yes/no, true/false). They can be sequenced together to form higher numbers, and the higher the number is, the more bits you need to display it. These numbers are called *binary*, as they are displayed by only using two digits. 

{{< video src="/images/A8/A8-26.webm" type="video/webm" preload="auto" >}}

The binary counter that I made during the Electronics design week uses binary numbers to display the amount of time the button has been pressed, until it reaches 15, after which it resets back to zero, as there are only 4 LEDs. To display the number 16, we would need a fifth LED, which we don't have. This the board is a 4-bit counter, which can handle numbers between 0-15. 

The controllers work in a similar way. An 8-bit microcontroller can operate with values that can be displayed with a maximum of 8 bits, which includes numbers between 0-255. To work data more than that the processor will have to to do more cycles, or it might not be able to handle it at all. I'm slowly getting my head around the theory of bits and bytes and everything else, but I still need to learn how it all works in practice, but I'm sure that will only come with time and actual practice.

The 1-series ATtiny microcontrollers can handle voltage between 1.8 to 5.5V. The standard USB ports are all 5 volts, which makes using ATtiny controllers very handy in devices that will be powered through the USB-ports, as there are no need for regulators. 

The frequency can also differ between microcontrollers. In the 1-series it is 20 MHz. Frequency is another term that has confused me for a long time. I have heard it in so many different contexts, from sounds to lights to radiowaves, I couldn't really put my finger on what *exactly* it was.

{{< youtube EZBjSkpE9Go >}}

The video linked above helped me realise, that frequency is actually a very descriptive word. It quite literally means how *frequently* an event occurs. Hertz is the unit used to measure the frequency of events, and means once per second. Changing the frequency of the wavelength can change the properties of light for example, and how we perceive it. 

In terms of the microcontrollers, 20MHz means the chip can run at maximum of 20MHz speed. Again, the practical implications of the chip frequency are still a bit hazy for me, but I am learning slow and steady. 

The D11C microcontroller is a 32-bit controller, which gives it a lot more processing power, and can be used in more complicated programs. However its operating voltage window is smaller, only 1.6 to 3.6 volts, which means when powering it from a USB port there should be a voltage regulator, which can regulate the incoming voltage and reduce it to 3.3 or lower. The maximum frequency of the chip is 48 MHz, which means it can be potentially almost 2.5 times fastere than the ATtiny 1-series. 

## Pinouts

When working with microcontrollers the pinout diagram is something that will become familiar very quickly. On the boards these pins will be connected to different input or output devices,  and it is important to be able to identify which pin which device is connected to, as they will be referred to in the code. Some pins also have different capacities, and depending on the case some devices can only be connected to specific pins. 

![Image 1: ATtiny 1614 pinout](/images/A10/A10-1.jpg)
ATtiny 1614 pinout

![Image 2: D11C pinout](/images/A10/A10-2.jpg)
D11C pinout

As you can see above, both of the microcontrollers have a total of 14 pins, one of which is reserved for ground, and one for voltage. The rest of the pins are usually considered GPIO (general purpose in/out) pins, and can be used to connect most of the typical in or output devices. The number of GPIO pins is also shown in the datasheet of the microcontroller, if they aren't shown in the pinout, like in the case of the D11C. 

![Image 3: D11C GPIO pins](/images/A10/A10-3.jpg)

Some other important pins that should be noted are the clock or oscillator pins, analog and digital pins, and TX/RX pins for receiving or sending data. 

## Programming

![Image 4: Arduino IDE](/images/A10/A10-4.jpg)

For programming the board we used Arduino IDE, a downloadable software that makes it easy to write code and upload it to the board to be executed. Upon startup the program will open an empty sketch, that has two functions called setup, and loop. As the names suggest the setup will be run once when the board is powered on or reset, and the loop will run continuously until the power is cut or the board is reset. 

![Image 5: Board and port](/images/A10/A10-5.jpg)

Before any code can be uploaded to the board there are at least two settings that need to be checked. On the tools menu, the board should be set correctly to the name of the microcontroller used, and the port where the board is connected selected correctly. 

![Image 6: Board manager](/images/A10/A10-6.jpg)

In order to get the options to be able to select the different microcontrollers they need to be setup in the board manager, which can be found in the preferences of the Arduino IDE. Depending on the situation it could also be possible that other steps need to be taken to be able to upload the code properly. These steps can usually be found in the documentation of the boards or controllers. 

## D11C board

![Image 7: Example code](/images/A10/A10-7.jpg)

The D11C board I borrowed had one red LED soldered on, and when I powered it on it was blinking rapidly. To get started with programming it I opened a Blink example code, that can be found on the File menu in Arduino.

``` bash
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(5, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(5, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
``` 

After the code is uploaded to the programmer the LED will start blinking turning on for one second, and then turning off for one second, and then starting the loop over again. 

{{< video src="/images/A10/A10-V1.webm" type="video/webm" preload="auto" >}}

After getting that working I wanted to try changing the frequency of the blinking, and I copied the code inside the loop function, and changed the delay to 300 (milliseconds), leaving only the last delay to 1000. 

{{< video src="/images/A10/A10-V2.webm" type="video/webm" preload="auto" >}}

After that I thought I would try display some morse code with the LED by adding in more instances of the code and editing the delay lengths. Are you able to read what the lights type out? (It runs through two cycles during the video.)

{{< video src="/images/A10/A10-V3.webm" type="video/webm" preload="auto" >}}

## ATtiny 1614

Unfortunately this week I ran out of time as I was catching up to some of the previous weeks assignments and documentation, so I didn't have time to try program my binary counter board to do something different. 

![Image 8: Binary counter](/images/A8/A8-27.jpg)

``` bash
/* 
/* 
 
  Binary counter

  Push of the button increases the count by one, current count shown in binary with the LEDs.
  Max count is 16, next increase resets the counter.

  Fiia Kitinoja
  6 March 2022

*/

int pin0=6; // LSB
int pin1=7;
int pin2=8;
int pin3=9; // MSB
int btn=10;

int count=0;
int buttonState;
int lastButtonState=LOW;

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // open the serial port at 9600 bps:
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(btn, INPUT_PULLUP);
  resetLEDs();
}

void loop() {
//  count++;
//  updateLEDs();
//  delay(1000);
  // put your main code here, to run repeatedly:
  int reading = digitalRead(btn);
  if(reading != lastButtonState) {
    // Reset debounce timer
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    if(reading != buttonState) {
      buttonState = reading;
      if(buttonState == HIGH) {
        count=count+1;
        updateLEDs();
      }
    }
  }
  lastButtonState = reading;
 }

void updateLEDs() {
  if (count >= 16) {
    resetLEDs();
    count=0;
  } else {
    digitalWrite(pin0, bitRead(count, 0));
    digitalWrite(pin1, bitRead(count, 1));
    digitalWrite(pin2, bitRead(count, 2));
    digitalWrite(pin3, bitRead(count, 3));
  }
}

void resetLEDs() {
  digitalWrite(pin0, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
}
```

{{< video src="/images/A8/A8-26.webm" type="video/webm" preload="auto" >}}

## Arduino workshop

Ohm's Law

Defines important relationship between three important electrical properties
V = Voltage, difference in electrical potential (Voltage)
I = Current rate of flow of electrons through specific point. Measured in Amps
R = Resistance Changes voltage like through a narrowing (Ohm's)

    V
  I   R

  Breadboard is kind of like a sketchbook for your electronics.

Digital signal is either on or off (0/1)

## Design Files

* [Binary Counter INO](/DesignFiles/A10/BIN-Count_V2.ino)





