+++
title = "Week 5 : Computer-controlled Cutting"
+++

## Week 5: Computer-controlled Cutting

For Assignment no. 5 our tasks were as follows:

* Characterize your lasercutter's focus, power, speed, rate and kerf.
* Cut something on the vinyl cutter.
* Design, lasercut a parametric press-fit kit that takes into account the kerf and can be assembled in multiple ways.
* Document all of the points above in a new page on your documentation website.
* Submit a link to the assignment page.

As I mentioned in previous week's assignment, I am somewhat familiar with the basics of lasercutting prior to the course. However my previous use of the machine has been very free flowing and organic so to say, instead of strictly parametric design, as we learned during this week. 

## Project of the week 

I've always loved the fusion of organic and mechanical, and I was inspired by a [Yu-Han Tseng's](https://yuhantyh.gitlab.io/digital-fabrication/assignments/computer_aided_design/computer_aided_design/), who's a fellow classmate, project from last week. She made a design based on Stranbeest's leg, and I'm excited to see the project brought to real life through laser cutting. 

I started looking up other lasercut projects with organic movement created by mechanical inputs. I found 
[Derek Hugger's](https://www.derekhugger.com/) organic motion sculptures beautifully captivating, although obviously way too complicated and detailed for me for this stage. It helped me decide however the direction for my little side-project, which would be animal movement. 

![Humming bird](/images/A5/hummingbird.gif)

Hummingbird Organic Motion Sculpture by Derek Hugger

![Carapace](/images/A5/carapace_derekhugger.gif)

Carapace Organic Motion Sculpture by Derek Hugger

I ended up choosing fish as my species for this project. I've always found the movement of fish relaxing and hypnotic, and it also relates to my final project, which I thought would be nice. I started by searching for more information about the way fish move, and found [this](https://www.researchgate.net/publication/51839717_Bending_continuous_structures_with_SMAs_A_novel_robotic_fish_design) paper, which was very helpful.

![Fish movement](/images/A5/movement-stills.jpg)

I used these movement stills to study the movement pattern. I liked the look of the top fish, which according to the paper imitates the subcarangiform swimming. I imported the screenshot to Procreate tho animate it to see the movement in action. In last week's assignment I showed some basic sketching on Procreate, but the app is capable of so much more. One of the best relatively new features it has is the Animation Assist. 

![Animation assist](/images/A5/A5-1.jpg)

By toggling on the animation assist Procreate will show a timeline of the animation where each layer of the drawing acts as a frame of the animation. There are other settings to help streamline the animation process from there-on. This feature isn't obviously as powerful as proper animation tools, however for this type of sketching it is perfect. I traced out the shape of each of the stills and added some more frames in-between to make the animation smoother. I am by no means an experienced animator, so the result isn't perfect, but it is most definitely very helpful for my project. 

![Fish movement 2](/images/A5/fish-moving.gif)

Below is a video of the progress of drawing the animation. At the end I created a layer, and set it to be the foreground frame, which means it will show throughout the animation, and marked the spots where the red dots were during each of the stage of the movement. 

{{< video src="/images/A5/fish-movement-progress.webm" type="video/webm" preload="auto" >}}

I simplified the pattern left by the dots into a more regular pattern, and tried to place the dots within those patterns, and then drew the spine following the dots. 

![Fish movement 3](/images/A5/spine-movement.gif)

I think there is probably a way to draw the above graph mathematically, but I would have to first learn how to do something like that. 

## Building the 3D Model

At this stage I decided to leave the movement study for a bit and move on to designing the body for the laser cutter. I wanted to translate the 2D cuts of the laser cutter into a 3D object, so I thought the best way to go about it was to have a 3D model of the fish, slice it into sections and assemble it using a bendy spine, that would allow movement. 

First I looked up species of fish that use subcarangiform swimming style, and rainbow trout ended up being one. I went on to Thingiverse and found [this](https://www.thingiverse.com/thing:108078) rainbow trout 3D model used as a base. 

![Rainbow trout 3d model](/images/A5/A5-2.jpg)

I decided to work in Fusion 360 this time, due to its more advanced features. The 3D model looked too detailed for what I wanted, so I decided to use it as a reference and sculpt the form from scratch. I created a rectangular form that was roughly the same length as the body of the fish. Then by moving the fish in front of or above the form I selected different edges on the form and pulled and pushed them until the shape was correct, and then I changed viewpoint to do the same thing. Below are some screenshots of the process. 

![Form 1](/images/A5/A5-3.jpg)

![Form 2](/images/A5/A5-4.jpg)

![Form 3](/images/A5/A5-5.jpg)

![Form 4](/images/A5/A5-6.jpg)

![Form 5](/images/A5/A5-7.jpg)

The whole process of designing the parts was about trial and error, since I have never made anything like this before. Looking back now, I realise many mistakes I did during the whole process. I drew a rectangle sketch in front of the fish, but I couldn't figure out how to create a pattern from the sketch without extruding. So I ended up extruding the sketch 0.1mm, which wasn't ideal, as that kinda threw off my parametrics. I had defined two parametrics at this point: MaterialThickness at 4mm, and PartSpacing at 10mm. I chose the body of the extruded part and created a rectangular pattern with spacing defined with parameters MaterialWidth + PartSpacing. After I had a row of rectangles going through the fish I selected each part individually, and used the combine tool to modify the rectangles into the shape of the fish. After that I extruded them with the Material Thickness parameter. 

![Form 6](/images/A5/A5-8.jpg)

![Form 7](/images/A5/A5-9.jpg)

![Form 8](/images/A5/A5-10.jpg)

The fins of the fish are already 2 dimensional, so to add them I just moved into the correct view, and traced them into a sketch, extruded them to the material thickness, and moved them on the correct place. 

![Form 9](/images/A5/A5-11.jpg)

![Form 10](/images/A5/A5-12.jpg)

I didn't want the fins to restrict the movement of the spine, so I wanted them to only be attached to one section of the body each. I added small protrusions into the body where the fins would be attached. At this point I made the mistake of not constraining the locations of the fins to the parts, so if I now change the material thickness, the fins will be in the wrong place. I then used the fins to cut the overlapping material from the protrusions. I couldn't figure out a way to attach the front fins due to their angle, so in the future I will have to think of that more. 

![Form 9](/images/A5/A5-13.jpg)

![Form 10](/images/A5/A5-14.jpg)

Lastly I created a circular sketch for the spine, and cut through all the parts. Then the 3D model was ready, and I needed to export a file for the laser cutter. I selected the manufacture tab and and used the arrange tool to arrange all the individual parts into a flat surface. I then went into sketch view and projected the faces, after which I exported the sketch into .dxf file that I could open in Illustrator. 

![Form 11](/images/A5/A5-15.jpg)

![Form 12](/images/A5/A5-16.jpg)

![Parameters](/images/A5/A5-30.jpg)

## Lasercutting

![Image 13](/images/A5/A5-17.jpg)

First I double checked the width of the material I had. I used 4mm plywood as my material that I got from the fablab. 

![Image 25](/images/A5/A5_29.jpg)

![Image 25](/images/A5/A5_31.jpg)

Before cutting my project I needed to calculate the kerf of the laser, which means the amount of material the laser will burn away while cutting. I set up a new drawing in Illustrator, and created 5 20x20mm rectangles to be cut. The laser will cut any outlines that are thinner than 0.01mm, so I made sure to set all the linewidths to that.

I clicked print in Illustrator to bring the drawing into Epilog's job manager to define settings for the cut. The only change I made in the Print dialog was to choose custom from the Media size

![Image 25](/images/A5/A5_32.jpg)

![Image 25](/images/A5/A5_33.jpg)

I selected Cutting 1/8" (3mm) settings, as that was the closest to my material. 

![Image 25](/images/A5/A5_34.jpg)

The original settings had 12% speed, but I changed it to 10% as my material was thicker than 3mm. However the laser left some burn marks on the material around the cuts, so in the future I would like to test the settings more to choose the optimal one. 

![Image 25](/images/A5/A5_35.jpg)

![Image 15](/images/A5/A5-19.jpg)

I named the project kerf_test and sent it to the lasercutter to be cut.

![Image 16](/images/A5/A5-20.jpg)

![Image 17](/images/A5/A5-21.jpg)

After cutting I measured the length of the rectangles, which came out to be 99mm. 5*20mm=100mm, which means altogether 1mm had been lost. 1mm/5=0.2mm, which came out to be the kerf value. 

I opened my 3D model in Fusion, changed the kerf value and spine values to be correct, and exported it again as a .dxf.

![Image 25](/images/A5/A5_36.jpg)

I opened the dxf file in Illustrator, and set the scale to Original size. I did forget to change the units from points to millimeters, which is why my first print failed and came out too small. 

![Image 25](/images/A5/A5_37.jpg)

![Image 25](/images/A5/A5_38.jpg)

Here I checked the measurement of a line in Illustrator, which was supposed to be the thickness of the material. It was 0.25mm so the lines were too small. The measurement was correct in Fusion so that told me that the issue was when I brought the file to Illustrator. Correcting the units to millimiters instead of points fixed the issue for me. 

![Image 25](/images/A5/A5_39.jpg)

![Image 18](/images/A5/A5-22.jpg)

![Image 19](/images/A5/A5-23.jpg)

![Image 20](/images/A5/A5-24.jpg)

I then brought the drawing to epilog dashboard and sent it to the laser to be cut.

![Image 21](/images/A5/A5-25.jpg)

I also made a little spacer to help me space out the sections of the fish correctly on the spine. 

![Image 22](/images/A5/A5-26.jpg)

![Image 23](/images/A5/A5-27.jpg)

Final version cut. On the second try the spine holes were too big. 

![Image 24](/images/A5/A5-28.jpg)

Squished fish body..

## Assembled fish

For the spine I used some 3mm diameter PLA plastic meant for 3D printing. Looking back I should've taken more care measuring the diameter of the plastic, as the holes in the parts ended up being about 1mm too big, and I had to use glue to put the parts in the correct places. I used the spacer shown above. 

![Image 22](/images/A5/A5_41.jpg)

Below you can see a photo and a video of the final product assembled. 

![Image 23](/images/A5/A5_40.jpg)

{{< video src="/images/A5/mechanical_fish_v1.webm" type="video/webm" preload="auto" >}}

In the future I would like to create a stand for the fish, with components that attach to it and using gears it would move in an organic way as I showed earlier on this page. There is still a lot to figure out to make that happen, so unfortunately I didn't have time this week to make that happen yet. 