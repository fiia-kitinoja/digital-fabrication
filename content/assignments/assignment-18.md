+++
title = "Applications and Implications"
Description = "Applications and Implications"
+++

## Final Project Masterpiece: Aquaponics Setup with a Water Monitoring System

#### What will it do?

My final project will cycle water between a fishtank, and a vegetable growbed. It will also control the lights above the growbed and the aquarium. It will monitor the waterquality through several sensors, and display them on a self-hosted web app in real time.

#### Who has done what beforehand?

[Assistive Aquaponics Fishtank by Teddy Warner](https://fabacademy.org/2021/labs/charlotte/students/theodore-warner/Final%20Project/final-project/)
[Aquapioneers by Guillaume Teyssie](http://archive.fabacademy.org/archives/2016/greenfablab/students/365/project03.html)

#### What will you design?

I will design/have designed:

- the frame
- the growbed
- the growlight fixture
- the sensor board
- the web application
- a variety of small components

#### What materials and components will be used?

TBC

#### Where will they come from?

TBC

#### How much will they cost?

TBC

#### What parts and systems will be made?

Growbed, frame, and light fixture, main controller board and sensor boards, bell siphon.

#### What processes will be used?

- 2D and 3D design
- Additive and subtractive fabrication processes
- Electronics design and production
- Microcontroller interfacing and programming
- System integration and packaging.

#### What questions need to be answered?

TBC

#### How will it be evaluated?

TBC
