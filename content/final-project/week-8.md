+++
title = "Final Project"
Description = "Electronics"
+++

## Electronics

Designing the electronics is definitely the hardest part for me, as I had basically no prior knowledge of electronic components and how to use them before starting the fab academy course. I've learned a lot during the electronics production and design weeks, but I've still been having a lot of trouble trying to figure out what exactly I need to make my aquaponics system 'smart'.

I started by trying to figure out what I need.

![Image 2: Schematics planning](/images/F5/F5_2.jpg)

### Controller

....

### Inputs

#### Temperature

![Image 3: DS18B20](/images/F8/F8-3.jpg)

For measuring the water temperature, I got a DS18B20 wire temperature sensor, in the submersible waterproof probe form.

![Image 4: DS18B20 wires](/images/F8/F8-4.jpg)

It has three wires, which are ground, digital pin connection, and voltage in, which should be between 3.3 and 5V. I followed [this](https://lastminuteengineers.com/ds18b20-arduino-tutorial/) tutorial to set it up. I installed two libraries on my arduino, the OneWire and DallasTemperature libraries. Below is the code.

```bash
#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugged into digital pin 2 on the Arduino
#define ONE_WIRE_BUS 7

// Setup a oneWire instance to communicate with any OneWire device
OneWire oneWire(ONE_WIRE_BUS);

// Pass oneWire reference to DallasTemperature library
DallasTemperature sensors(&oneWire);

void setup(void)
{
  sensors.begin();  // Start up the library
  Serial.begin(9600);
}

void loop(void)
{
  // Send the command to get temperatures
  sensors.requestTemperatures();

  //print the temperature in Celsius
  Serial.print("Temperature: ");
  Serial.print(sensors.getTempCByIndex(0));
  Serial.print((char)176);//shows degrees character
  Serial.print("C  |  ");

  //print the temperature in Fahrenheit
  Serial.print((sensors.getTempCByIndex(0) * 9.0) / 5.0 + 32.0);
  Serial.print((char)176);//shows degrees character
  Serial.println("F");

  delay(500);
}

```

![Image 5: Input sensors](/images/F8/F8-5.jpg)

I made the board above to connect the temperature sensor into. It has an ATtiny412 microcontroller. After finishing the soldering I tried to program it, but I was having trouble compiling the code for the ATtiny412. After doing some troubleshooting I realised that the code was able to compile for the ATtiny 1614 and 3216 boards, so I think the 412 doesn't have enough memory for the libraries needed.

Matti was able to find a [reduced size library](https://www.arduino.cc/reference/en/libraries/ds18b20_int/) for the sensor, that was working with the 412, however it doesn't have all the capabilites of the other library. I also had other sensors that weren't working for the 412, so I decided to at the moment get them working with an Arduino Uno, so that I would have all the code working and could then figure out what I need for the board.

![Image 6a: Wiring diagram](/images/F8/F8-6a.jpg)

(c) LastMinuteEngineers.com

I followed the wiring instructions from the tutorial and connected it to my Arduino with a resistor. I ran the code I pasted above and was able to get temperature readings.

![Image 6: Wiring](/images/F8/F8-6.jpg)

![Image 7: Temperatures](/images/F8/F8-7.jpg)

I then grabbed a glass of warm water to test the probe in the water, and the temperature went up. I'm not currently sure how accurate the sensor is, or if it needs to be calibrated first, but it is at least in the correct ballpark.

![Image 8: Water](/images/F8/F8-8.jpg)

![Image 9: Water temperatures](/images/F8/F8-9.jpg)

<!-- I need a temperature sensor, that is connected to a probe that is submerges in the tank, and measures the water temperature, which will be shown on the display screen. With that I will be able to easily monitor the temperature of the water and make sure it is suitable for the fish.

I started by trying to figure out how to measure temperature.

![Image 3: Parts](/images/F5/F5_3.jpg)

Components used:

* [LOLIN D1 mini](https://www.wemos.cc/en/latest/d1/d1_mini.html)
* [BMP280](https://learn.adafruit.com/adafruit-bmp280-barometric-pressure-plus-temperature-sensor-breakout)
* Breadboard
* Jumper wires

![Image 2: Wiring test](/images/F5/F5_4.jpg) -->

<!-- ``` bash
/1***************************************************************************
  This is a library for the BMP280 humidity, temperature & pressure sensor

  Designed specifically to work with the Adafruit BMEP280 Breakout
  ---->

  <!-- These sensors use I2C or SPI to communicate, 2 or 4 pins are required
  to interface.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>

#define BMP_SCK  (13)
#define BMP_MISO (12)
#define BMP_MOSI (11)
#define BMP_CS   (10)

Adafruit_BMP280 bmp; // I2C
//Adafruit_BMP280 bmp(BMP_CS); // hardware SPI
//Adafruit_BMP280 bmp(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);

void setup() {
  Serial.begin(9600);
  Serial.println(F("BMP280 test"));

  if (!bmp.begin(0x76)) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }

  /* Default settings from datasheet. */
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
}

void loop() {
    Serial.print(F("Temperature = "));
    Serial.print(bmp.readTemperature());
    Serial.println(" *C");

    Serial.print(F("Pressure = "));
    Serial.print(bmp.readPressure());
    Serial.println(" Pa");

    Serial.print(F("Approx altitude = "));
    Serial.print(bmp.readAltitude(1013.25)); /* Adjusted to local forecast! */
    Serial.println(" m");

    Serial.println();
    delay(2000);
}
```  -->
<!--
{{< video src="/images/F5/F5_1.webm" type="video/webm" preload="auto" >}}

I followed [this](http://www.esp32learning.com/code/esp32-and-bmp280-sensor-example.php) example, and got it working after doing some troubleshooting and some changes to the code.  -->

<!--
#### Nutrients

Measured through electrical conductivity sensor -->

#### pH

![Image 10: SEN0161](/images/F8/F8-10.jpg)

Kris ordered some SEN0161 pH sensors for us to the lab, so that is what I'm using to test the pH of the water in my aquaponics system. DFRobot have a great [tutorial](https://wiki.dfrobot.com/PH_meter_SKU__SEN0161_) on their website, which I followed. At first I was following [this](https://wiki.dfrobot.com/Gravity__Analog_pH_Sensor_Meter_Kit_V2_SKU_SEN0161-V2) tutorial from then, before realising that was for version 2, and we have a version 1. I didn't have much issues with the pH sensor, I wired it according to the tutorial instructions.

![Image 11: wiring](/images/F8/F8-11.jpg)

(c) DFRobot

![Image 12: wiring](/images/F8/F8-12.jpg)

Another class mate [Karl](https://karlpalo.gitlab.io/fablab2022/) has access to the chemistry department and was able to get us the buffer solutions to calibrate the pH sensor correctly.

```bash

/*
 # This sample code is used to test the pH meter V1.0.
 # Editor : YouYou
 # Ver    : 1.0
 # Product: analog pH meter
 # SKU    : SEN0161
*/
#define SensorPin A0            //pH meter Analog output to Arduino Analog Input 0
#define Offset 0.00            //deviation compensate
#define LED 13
#define samplingInterval 20
#define printInterval 800
#define ArrayLenth  40    //times of collection
int pHArray[ArrayLenth];   //Store the average value of the sensor feedback
int pHArrayIndex=0;
void setup(void)
{
  pinMode(LED,OUTPUT);
  Serial.begin(9600);
  Serial.println("pH meter experiment!");    //Test the serial monitor
}
void loop(void)
{
  static unsigned long samplingTime = millis();
  static unsigned long printTime = millis();
  static float pHValue,voltage;
  if(millis()-samplingTime > samplingInterval)
  {
      pHArray[pHArrayIndex++]=analogRead(SensorPin);
      if(pHArrayIndex==ArrayLenth)pHArrayIndex=0;
      voltage = avergearray(pHArray, ArrayLenth)*5.0/1024;
      pHValue = 3.5*voltage+Offset;
      samplingTime=millis();
  }
  if(millis() - printTime > printInterval)   //Every 800 milliseconds, print a numerical, convert the state of the LED indicator
  {
    Serial.print("Voltage:");
        Serial.print(voltage,2);
        Serial.print("    pH value: ");
    Serial.println(pHValue,2);
        digitalWrite(LED,digitalRead(LED)^1);
        printTime=millis();
  }
}
double avergearray(int* arr, int number){
  int i;
  int max,min;
  double avg;
  long amount=0;
  if(number<=0){
    Serial.println("Error number for the array to avraging!/n");
    return 0;
  }
  if(number<5){   //less than 5, calculated directly statistics
    for(i=0;i<number;i++){
      amount+=arr[i];
    }
    avg = amount/number;
    return avg;
  }else{
    if(arr[0]<arr[1]){
      min = arr[0];max=arr[1];
    }
    else{
      min=arr[1];max=arr[0];
    }
    for(i=2;i<number;i++){
      if(arr[i]<min){
        amount+=min;        //arr<min
        min=arr[i];
      }else {
        if(arr[i]>max){
          amount+=max;    //arr>max
          max=arr[i];
        }else{
          amount+=arr[i]; //min<=arr<=max
        }
      }//if
    }//for
    avg = (double)amount/(number-2);
  }//if
  return avg;
}

```

![Image 13: Initial values](/images/F8/F8-13.jpg)

I uploaded the code to the Arduino and then opened the serial monitor to see the values. The code prints out the values for the voltage and the pH. As you can see above the pH is 6.62, instead of 7.00, like the buffer solution should be. In the sample code there is a space for offset, which should be able to correct the discrepancy. I defined offset to be 0.38 and uploaded the code again.

![Image 14: Second values](/images/F8/F8-14.jpg)

The pH was now showing correctly, so the next step was to switch solutions.

![Image 15: 4.00 solution](/images/F8/F8-15.jpg)

![Image 16: third values](/images/F8/F8-16.jpg)

The next solution I put the probe in was 4.00 buffer solution. As you can see the pH value isn't showing correctly. I think the voltage values need to be mapped again for the different voltage values, which is something I will have to look into later. For reference the 10.00 solution was displaying as 11.88.

![Image 17: not airtight](/images/F8/F8-17.jpg)

After I was done testing the pH sensor I put it back inside its cap and inside the box it came in. After coming back to it after a couple days it had this frosting on its side. I assume the seal wasn't airtight anymore and some of the solution was able to escape and it dried after coming in contact with the air. I'll have to figure out how to keep the sensor fine after using it.

#### Soil moisture

![Image 18: Moisture sensor](/images/F8/F8-18.jpg)

I also got this HW-390 moisture sensor to add to my sensor collection. I followed [this](https://how2electronics.com/interface-capacitive-soil-moisture-sensor-arduino/) tutorial to set it up.

```bash

const int AirValue = 470;   //you need to replace this value with Value_1
const int WaterValue = 224;  //you need to replace this value with Value_2
int soilMoistureValue = 0;
int soilmoisturepercent=0;
void setup() {
  Serial.begin(9600); // open serial port, set the baud rate to 9600 bps
}
void loop() {
soilMoistureValue = analogRead(A0);  //put Sensor insert into soil
Serial.println(soilMoistureValue);
soilmoisturepercent = map(soilMoistureValue, AirValue, WaterValue, 0, 100);
if(soilmoisturepercent >= 100)
{
  Serial.println("100 %");
}
else if(soilmoisturepercent <=0)
{
  Serial.println("0 %");
}
else if(soilmoisturepercent >0 && soilmoisturepercent < 100)
{
  Serial.print(soilmoisturepercent);
  Serial.println("%");

}
  delay(250);
}

```

I calibrated the sensor by putting in the AirValue and WaterValue after measuring the value in both, and it was working.

![Image 19: Moisture sensor](/images/F8/F8-19.jpg)

#### Real Time Clock

![Image 20: PCF8523](/images/F8/F8-20.jpg)

I want to be able to control the lights of the aquarium and the growbed separately, and preferably through the controller and have an easy way to monitor them and change the times if necessary. Kris ordered some of these Adafruit PCF8523 RTC components to the lab, so I took one of them. It has an input for a CR1220 coin cell battery, which means it will stay powered on even if the board itself turns off. This is great so that in case of a power outage for example the time will still be correct when the power comes back on instead of being reset.

Adafruit has a comprehensive [tutorial](https://learn.adafruit.com/adafruit-pcf8523-real-time-clock) about using the RTC, which was very easy to follow.

![Image 21: PCF8523](/images/F8/F8-21.jpg)

The RTC connects to the SDA and SCL pins on the Arduino, as well as 5V and ground. I wired up the component and downloaded the RTClib library from Adafruit, then opened the PCF8523 example code.

```bash

// Date and time functions using a PCF8523 RTC connected via I2C and Wire lib
#include "RTClib.h"

RTC_PCF8523 rtc;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

void setup () {
  Serial.begin(57600);

#ifndef ESP8266
  while (!Serial); // wait for serial port to connect. Needed for native USB
#endif

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }

  if (! rtc.initialized() || rtc.lostPower()) {
    Serial.println("RTC is NOT initialized, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
    //
    // Note: allow 2 seconds after inserting battery or applying external power
    // without battery before calling adjust(). This gives the PCF8523's
    // crystal oscillator time to stabilize. If you call adjust() very quickly
    // after the RTC is powered, lostPower() may still return true.
  }

  // When time needs to be re-set on a previously configured device, the
  // following line sets the RTC to the date & time this sketch was compiled
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));

  // When the RTC was stopped and stays connected to the battery, it has
  // to be restarted by clearing the STOP bit. Let's do this to ensure
  // the RTC is running.
  rtc.start();

   // The PCF8523 can be calibrated for:
  //        - Aging adjustment
  //        - Temperature compensation
  //        - Accuracy tuning
  // The offset mode to use, once every two hours or once every minute.
  // The offset Offset value from -64 to +63. See the Application Note for calculation of offset values.
  // https://www.nxp.com/docs/en/application-note/AN11247.pdf
  // The deviation in parts per million can be calculated over a period of observation. Both the drift (which can be negative)
  // and the observation period must be in seconds. For accuracy the variation should be observed over about 1 week.
  // Note: any previous calibration should cancelled prior to any new observation period.
  // Example - RTC gaining 43 seconds in 1 week
  float drift = 43; // seconds plus or minus over oservation period - set to 0 to cancel previous calibration.
  float period_sec = (7 * 86400);  // total obsevation period in seconds (86400 = seconds in 1 day:  7 days = (7 * 86400) seconds )
  float deviation_ppm = (drift / period_sec * 1000000); //  deviation in parts per million (μs)
  float drift_unit = 4.34; // use with offset mode PCF8523_TwoHours
  // float drift_unit = 4.069; //For corrections every min the drift_unit is 4.069 ppm (use with offset mode PCF8523_OneMinute)
  int offset = round(deviation_ppm / drift_unit);
  // rtc.calibrate(PCF8523_TwoHours, offset); // Un-comment to perform calibration once drift (seconds) and observation period (seconds) are correct
  // rtc.calibrate(PCF8523_TwoHours, 0); // Un-comment to cancel previous calibration

  Serial.print("Offset is "); Serial.println(offset); // Print to control offset

}

void loop () {
    DateTime now = rtc.now();

    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);
    Serial.print(" (");
    Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
    Serial.print(") ");
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();

    Serial.print(" since midnight 1/1/1970 = ");
    Serial.print(now.unixtime());
    Serial.print("s = ");
    Serial.print(now.unixtime() / 86400L);
    Serial.println("d");

    // calculate a date which is 7 days, 12 hours and 30 seconds into the future
    DateTime future (now + TimeSpan(7,12,30,6));

    Serial.print(" now + 7d + 12h + 30m + 6s: ");
    Serial.print(future.year(), DEC);
    Serial.print('/');
    Serial.print(future.month(), DEC);
    Serial.print('/');
    Serial.print(future.day(), DEC);
    Serial.print(' ');
    Serial.print(future.hour(), DEC);
    Serial.print(':');
    Serial.print(future.minute(), DEC);
    Serial.print(':');
    Serial.print(future.second(), DEC);
    Serial.println();

    Serial.println();
    delay(3000);
}

```

![Image 22: PCF8523](/images/F8/F8-22.jpg)

The code was working as it should, and it was printing out the current time and some other information every three seconds.

#### Putting the inputs together

After getting all the inputs to work separately, I wanted to connect them all to the arduino at the same time, and have a nice printout of all the information I needed, and nothing else.

```bash

// pins
#define moisturePin 0
#define pHSensorPin 1
#define tempPin 7

// RTC
#include "RTClib.h"
RTC_PCF8523 rtc;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// pH
#define pHOffset 0.00
#define pHsamplingInterval 20
#define pHArrayLenth  40    //times of collection
int pHArray[pHArrayLenth];   //Store the average value of the sensor feedback
int pHArrayIndex=0;

// Moisture sensor
const int AirValue = 507;   //you need to replace this value with Value_1
const int WaterValue = 224;  //you need to replace this value with Value_2
int soilMoistureValue = 0;
int soilmoisturepercent=0;

// Temperature
#include <OneWire.h>
#include <DallasTemperature.h>
OneWire oneWire(tempPin);
DallasTemperature sensors(&oneWire);



void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  setupTime();
  sensors.begin(); // start up temperature library
}

void loop() {
      DateTime now = rtc.now();
  // put your main code here, to run repeatedly:
  checkTime();
  checkTemp();
  checkpH();
  checkMoisture();
  Serial.println();


  delay(3000);
}

void setupTime() {
  #ifndef ESP8266
  while (!Serial); // wait for serial port to connect. Needed for native USB
#endif

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }

  if (! rtc.initialized() || rtc.lostPower()) {
    Serial.println("RTC is NOT initialized, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
    //
    // Note: allow 2 seconds after inserting battery or applying external power
    // without battery before calling adjust(). This gives the PCF8523's
    // crystal oscillator time to stabilize. If you call adjust() very quickly
    // after the RTC is powered, lostPower() may still return true.
  }

  // When time needs to be re-set on a previously configured device, the
  // following line sets the RTC to the date & time this sketch was compiled
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));

  // When the RTC was stopped and stays connected to the battery, it has
  // to be restarted by clearing the STOP bit. Let's do this to ensure
  // the RTC is running.
  rtc.start();

   // The PCF8523 can be calibrated for:
  //        - Aging adjustment
  //        - Temperature compensation
  //        - Accuracy tuning
  // The offset mode to use, once every two hours or once every minute.
  // The offset Offset value from -64 to +63. See the Application Note for calculation of offset values.
  // https://www.nxp.com/docs/en/application-note/AN11247.pdf
  // The deviation in parts per million can be calculated over a period of observation. Both the drift (which can be negative)
  // and the observation period must be in seconds. For accuracy the variation should be observed over about 1 week.
  // Note: any previous calibration should cancelled prior to any new observation period.
  // Example - RTC gaining 43 seconds in 1 week
  float drift = 43; // seconds plus or minus over oservation period - set to 0 to cancel previous calibration.
  float period_sec = (7 * 86400);  // total obsevation period in seconds (86400 = seconds in 1 day:  7 days = (7 * 86400) seconds )
  float deviation_ppm = (drift / period_sec * 1000000); //  deviation in parts per million (μs)
  float drift_unit = 4.34; // use with offset mode PCF8523_TwoHours
  // float drift_unit = 4.069; //For corrections every min the drift_unit is 4.069 ppm (use with offset mode PCF8523_OneMinute)
  int offset = round(deviation_ppm / drift_unit);
  // rtc.calibrate(PCF8523_TwoHours, offset); // Un-comment to perform calibration once drift (seconds) and observation period (seconds) are correct
  // rtc.calibrate(PCF8523_TwoHours, 0); // Un-comment to cancel previous calibration

  Serial.print("Offset is "); Serial.println(offset); // Print to control offset
}

void checkTime() {
    DateTime now = rtc.now();

    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);
    Serial.print(" (");
    Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
    Serial.print(") ");
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();
}

void checkMoisture() {
  soilMoistureValue = analogRead(moisturePin);  //put Sensor insert into soil
  soilmoisturepercent = map(soilMoistureValue, AirValue, WaterValue, 0, 100);
  if(soilmoisturepercent >= 100)
  {
    Serial.print("Soil moisture:");
    Serial.println("100 %");
    Serial.println();
  }
  else if(soilmoisturepercent <=0)
  {
    Serial.print("Soil moisture:");
    Serial.println("0 %");
    Serial.println();
  }
  else if(soilmoisturepercent >0 && soilmoisturepercent < 100)
  {
    Serial.print("Soil moisture:");
    Serial.print(soilmoisturepercent);
    Serial.println("%");
    Serial.println();

  }
}

double avergearray(int* arr, int number){
  int i;
  int max,min;
  double avg;
  long amount=0;
  if(number<=0){
    Serial.println("Error number for the array to avraging!/n");
    return 0;
  }
  if(number<5){   //less than 5, calculated directly statistics
    for(i=0;i<number;i++){
      amount+=arr[i];
    }
    avg = amount/number;
    return avg;
  }else{
    if(arr[0]<arr[1]){
      min = arr[0];max=arr[1];
    }
    else{
      min=arr[1];max=arr[0];
    }
    for(i=2;i<number;i++){
      if(arr[i]<min){
        amount+=min;        //arr<min
        min=arr[i];
      }else {
        if(arr[i]>max){
          amount+=max;    //arr>max
          max=arr[i];
        }else{
          amount+=arr[i]; //min<=arr<=max
        }
      }//if
    }//for
    avg = (double)amount/(number-2);
  }//if
  return avg;
}

void checkpH() {
    static unsigned long pHsamplingTime = millis();
    static unsigned long printTime = millis();
  static float pHValue,voltage;
  if(millis()-pHsamplingTime > pHsamplingInterval)
  {
      pHArray[pHArrayIndex++]=analogRead(pHSensorPin);
      if(pHArrayIndex==pHArrayLenth)pHArrayIndex=0;
      voltage = avergearray(pHArray, pHArrayLenth)*5.0/1024;
      pHValue = 3.5*voltage+pHOffset;
      pHsamplingTime=millis();
  }
        Serial.print("pH value: ");
    Serial.println(pHValue,2);
        printTime=millis();
}

void checkTemp() {
  Serial.print("Temperature: ");
  Serial.print(sensors.getTempCByIndex(0));
  Serial.print((char)176);//shows degrees character
  Serial.println("C ");
}

```

![Image 23: All inputs](/images/F8/F8-23.jpg)

I created functions for all the different sensors, and then called them in the loop function, so that the code would be a bit easier to edit and troubleshoot when all the inputs had their own sections. The next step will be to get the inputs from the Arduino to the Raspberry pi via I2C.

#### Camera

Raspberry Pi (clone) serial camera.

Dimensions:
x 24.7 mm
y 24 mm
z 8.7 mm

Lens 6.1 mm

3mm acrylic

....

### Outputs

#### Screen

....

#### Aquarium and Grow lights

Light is important for all plants and their growth, and therefore I will need to install growlights for both the growbed, and the aquarium, because I want to include some low maintanence water plants within the fishtank as well. LED lights are a good option, as they have a low energy consumption, and a long lifetime, among other things.

First I need to figure out what amount of light will I need for growing my vegetables in the growbed. I will mostly be growing leafy greens and herbs, so I won't need as much light as some other plants like tomatoes would require. After doing some research I found that a good amount of light for my needs would be about **25-30 watts per square foot**, which in square cm would be about 923 cm2. I am envisioning my growbed to be about 80cm x 40cm big. Therefore I need to do some calculations.

```bash
80 cm * 40 cm = 3200 cm2
3200 cm2 / 923 cm2 = 3.46696
3.46696 * 25 W = 87 Watts
3.46696 * 30 W = 104 Watts
```

According to the above calculations for the growbed I would need between 87 to 104 watts of light.

I got some LED strips from Fablab, so currently I am seeing if that is something I can use for my project.

![Image 1: LED strip](/images/F5/F5_LG1.jpg)

I managed to find the [Digi-key entry](https://www.digikey.fi/en/products/detail/american-bright-optoelectronics-corporation/AB-FC01240-19712-8A1/9677836) for the lights, from where I found the [datasheet](https://www.americanbrightled.com/pdffiles/flexible-lighting/strip-lights/AB-FC.pdf). On the front page it says that the lights have IP65 waterproof rating. I did some googling regarding the rating, and the first number refers to solid particle protection, and is rated between 0 to 6. Therefore the lights are considered dust-tight. The second digit refers to liquid ingress protection, and is rated between 0 and 9K. The value 5 means that the lights have protection against water jets. In addition I did some googling and found multiple aquarium lights that were rated IP65, which gives me confidence that these lights should work in my setup!

The next step was to figure out if they are powerful enough, and how many strips of them I would need. The LEDs we have at the fablab are 12V, and towards the end of the datasheet there is some information about their power.

![Image 2: LED characteristics](/images/F5/F5_LG2.jpg)

```bash
87 / 8 = 10.9
104 / 8 = 13

roughly 12m of LED strip

12 / 0.8 = 15
```

According to the above calculations I would need roughly 12m of the LED strip, which would come out to about 15 strips of LEDs. On the Digi-Key website the price comes up to 63e, for a roll of 5m. Therefore even if I went slightly lower on the recommended amount of watts, the cost would still come to over 120e just for the grow lights.

I would like to try to keep the costs as low as possible, so if I have done all the math correctly, I'm thinking of looking into building the LED strips myself, and see if that would be cheaper.

{{< youtube 3c-cAjx8jt8 >}}

The LEDs used in the above video are 3.5 W LEDs, which would come out to roughly about 30 LEDs for the growbed.

https://www.instructables.com/Introduction-to-LED-grow-lights/

#### Timelapse

....

### Wi-Fi Connection

....

<!-- feed power circuits 12V ´regulator transistor on or of

hello leds t412

output devices week

try make silicone

adafruit PCF8523

NHQ

attiny 412 thermistor
networking week

Molding and casting - waterproofing
input week - tempertatu
output week - led
networking - main board

spectroscopy -->
